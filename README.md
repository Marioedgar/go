## Test playground for the Go programming language

### Binary Search Algorithm

```

go run binarySearch/main.go -needle=123

```

### Reverse a String

```

go run reversString/main.go -string='testing 123, this is a test sentence!'

```

### Start a WebServer

```

go run webServer/main.go

```
### Start a REST API powered by Echo and MongoDB

```
This requires a MongoDB instance to be running at localhost:27017 with a database named: go-test

go run echoServer/main.go echoServer/mongoService.go

```