package main

import (
	"flag"
	"fmt"
)

func main() {

	string := flag.String("string", "This is a test sentence!", "help message for needle")

	flag.Parse()

	reversed := Reverse(*string)

	fmt.Println("input string: ", *string)
	fmt.Println("reversed string: ", reversed)
}

func Reverse(str string) string {
	if str == "" {
		return ""
	}
	return Reverse(str[1:]) + str[0:1]
}
