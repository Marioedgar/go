package main

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

const databaseUrl = "mongodb://localhost:27017"
const databaseName = "go-test"
const collectionName = "test-collection"

type User struct {
	id    string
	first string
	last  string
	age   string
}

func getCollection() mgo.Collection {
	session, _ := mgo.Dial(databaseUrl)
	collection := session.DB(databaseName).C(collectionName)
	return *collection
}

func GetUser(context echo.Context) error {
	collection := getCollection()
	id := context.Param("id")
	jsonBody := make(map[string]string)
	retrieveError := collection.Find(bson.M{"_id": id}).One(&jsonBody)
	if retrieveError != nil {
		fmt.Println("retrieveError: ", retrieveError)
	}
	delete(jsonBody, "_id")
	return context.JSON(http.StatusOK, jsonBody)
}

func SaveUser(context echo.Context) error {
	collection := getCollection()
	id := context.Param("id")
	jsonBody := make(map[string]string)
	decodeErr := json.NewDecoder(context.Request().Body).Decode(&jsonBody)
	if decodeErr != nil {
		fmt.Println("decodeErr: ", decodeErr)
	}
	jsonBody["_id"] = id
	insertErr := collection.Insert(jsonBody)
	if insertErr != nil {
		fmt.Println("insertErr: ", insertErr)
		return context.String(http.StatusConflict, "A user already exists with the given id")
	}
	return context.String(http.StatusOK, "Success!")
}

func UpdateUser(context echo.Context) error {
	collection := getCollection()
	jsonBody := make(map[string]string)
	id := context.Param("id")
	decodeErr := json.NewDecoder(context.Request().Body).Decode(&jsonBody)
	if decodeErr != nil {
		fmt.Println("decodeErr: ", decodeErr)
	}
	jsonBody["_id"] = context.Param("id")
	insertErr := collection.Update(bson.M{"_id": id}, jsonBody)
	if insertErr != nil {
		fmt.Println("insertErr: ", insertErr)
	}
	return context.String(http.StatusOK, "Success!")

}

func DeleteUser(context echo.Context) error {
	collection := getCollection()
	id := context.Param("id")
	insertErr := collection.Remove(bson.M{"_id": id})
	if insertErr != nil {
		fmt.Println("insertErr: ", insertErr)
	}
	return context.String(http.StatusOK, "Success!")
}
