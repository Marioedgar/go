package main

import (
	"github.com/labstack/echo"
	"net/http"
)

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Welcome to the echo / mongodb app! \n\nGET a user: GET /users/:id \nAdd a user: POST /users/:id \nUpdate a user: PUT /users/:id \nDelete a user: DELETE /users/:id")
	})
	e.POST("/users/:id", SaveUser)
	e.GET("/users/:id", GetUser)
	e.PUT("/users/:id", UpdateUser)
	e.DELETE("/users/:id", DeleteUser)
	e.Logger.Fatal(e.Start(":1323"))
}
