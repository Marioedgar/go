package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func serveStaticPage(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	// Get current working directory
	dir, err := os.Getwd()
	if err != nil {
		fmt.Print(err)
	}
	// Build path to markup template
	var filePath bytes.Buffer
	filePath.WriteString(dir)
	filePath.WriteString("/webServer/template.html")
	markup, err := ioutil.ReadFile(filePath.String())
	if err != nil {
		fmt.Print(err)
	}
	resolvedMarkup := string(markup)
	// Replace all of the variables in the template
	for k, v := range r.Form {
		var key bytes.Buffer
		key.WriteString("{{")
		key.WriteString(k)
		key.WriteString("}}")
		resolvedMarkup = strings.Replace(resolvedMarkup, key.String(), strings.Join(v, ""), -1)
	}
	// Send data to Client
	fmt.Fprintf(w, resolvedMarkup)
}

func main() {
	http.HandleFunc("/", serveStaticPage)    // set router
	err := http.ListenAndServe(":8000", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
