package main

import (
	"flag"
	"fmt"
	"sort"
)

func main() {

	needle := flag.Int("needle", 4444444444444, "help message for needle")

	flag.Parse()

	array := []int{
		1, 526, 4444444444444, 7578, 2332, 12345, 56222, 445, 7557, 8, 433323, 23343, 3, 33324324,
		42, 526, 12435, 7578, 23232, 1, 6, 5, 75557, 8, 4332433, 23343, 3, 312113324324,
		2, 5246, 435, 221222, 23232, 123445, 5446, 15, 75557, 8, 4334323, 23343, 3123, 23,
		62, 5222246, 12435, 7578, 23232, 123445, 56, 5, 75557, 8, 324323, 23343, 311, 666,
		212, 521246, 12435, 7578, 23244432, 1244443445, 5446, 25, 75557, 8, 433, 243, 33, 55, 123,
		352325, 123334, 12234, 12, 4, 12, 423, 214, 412, 214, 324576, 1244, 235, 2462351, 234, 14, 332342,
		1234, 124, 124, 124, 124, 124, 124, 12, 4, 124, 2, 65, 436, 4, 23423475, 4635, 23, 47, 54635, 454, 35241,
		3, 3435, 6453241, 32, 435445, 6352413, 2435, 46563, 54231, 243546576, 7463, 5241, 3546576, 874, 6534,
		234, 23423, 4234, 2342, 35426, 463463456, 234213423, 2, 22, 2, 34, 4, 5, 65, 3, 234, 235, 23, 523, 423,
		1111, 12222, 4, 333334, 45, 4, 5, 6, 644444, 7, 2, 8, 8222, 7543, 2, 46524, 54653, 52, 46544325, 4135465, 6546352, 46576,
	}

	// sort first
	sort.Ints(array)

	fmt.Println("Running binarySearch test with needle: ", *needle)

	result := BinarySearch(array, *needle)

	fmt.Println("\n\n", result)
}

func BinarySearch(arr []int, needle int) string {

	if len(arr) <= 1 {
		return "404 no needle found :("
	}

	midpoint := len(arr) / 2
	foundValue := arr[midpoint]

	fmt.Print("\narray length: ", len(arr))
	fmt.Print(". current value: ", foundValue)

	if foundValue == needle {
		return "found needle :)"
	}
	if foundValue < needle {
		return BinarySearch(arr[midpoint:], needle)
	}
	return BinarySearch(arr[:midpoint], needle)

}
